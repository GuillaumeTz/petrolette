// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

PTL.col = {
  del: function($column) {

    const $panel = $column.parent();

    const nbOfColumnsInTab = $panel.find('.column').length;

    $column.hide('fast', function() {

      if (nbOfColumnsInTab <= 2) {
        $panel.find('.col-del').addClass('ui-state-disabled');
      }

      $(this).remove();

      PTL.tab.saveTabs();

    });
  },
  add: function(colIndex) {

    const $colButtons = $('<div>')
          .attr('class', 'colButtons flexBox');

    const $colLegend = $('<legend>')
        .attr('class', 'colLegend legend-col unique translate')
        .data('title', 'Column')
        .data('content', 'Column')
        .text(PTL.tr('Column'));

    const $column = $('<ul>')
          .attr('class', 'column')
          .append($colButtons);

    const $colNewButton = $('<button>')
          .attr('title', PTL.tr('Add a column'))
          .data('title', 'Add a column')
          .attr('class', 'icon-plus twin translate last')
          .data('colIndex', colIndex)
          .button();

    const $colDelButton = $('<button>')
          .attr('title', PTL.tr('Remove this column'))
          .data('title', 'Remove this column')
          .attr('class', 'col-del icon-minus twin')
          .data('colIndex', colIndex)
          .button();

    $colDelButton.bind('click', function() {

      const $column = $(this).parent().parent(),
            $feedsInCol = $column.find('.feed'),
            nbOfFeedsInCol = $feedsInCol.length;
      
      if (nbOfFeedsInCol < 1) {
        PTL.col.del($column);
      } else {
        PTL.dialog.killColumn($(this));
      }

    });

    $colNewButton.bind('click', function() {

      const $column = $(this).parent().parent(),
            $panel = $column.parent(),
            colIndex = $panel.find('.column').index($column);

      const $newColumn = PTL.col.add(colIndex, true);

      $panel.find('button.col-del').removeClass('ui-state-disabled');

      $newColumn.insertAfter($column);

    });

    $column.sortable({
      cursor: 'move',
      handle: ".feedHandle",
      connectWith: ".column",
      cursorAt: {top: 10, left: 32},
      receive: function(e, ui) {
        if (ui.helper)
          ui.helper.first().removeAttr('style'); // undo styling set by jqueryUI
      },
      helper: function (e, item) { //create custom helper
        if (!item.hasClass('selected')) item.addClass('selected');
        // clone selected items before hiding
        const $elements = $('.selected').not('.ui-sortable-placeholder').clone();
        //hide selected items
        item.siblings('.selected').addClass('hidden');
        const $helper = $('<ul class="feedHelper">');
        return $helper.append($elements);
      },
      start: function (e, ui) {
        // Drag begins
        const $elements = ui.item.siblings('.selected.hidden').not('.ui-sortable-placeholder');
        // Store the selected items to item being dragged
        ui.item.data('items', $elements);
        // Size the placeHolder
        $('.ui-sortable-placeholder').css('height', ui.item.height());
      },
      update: function (e, ui) {
        //manually add the selected items before the one actually being dragged
        ui.item.before(ui.item.data('items'));
      },
      stop: function (e, ui) {
        //show the selected items after the operation
        ui.item.siblings('.selected').removeClass('hidden');
        //unselect since the operation is complete
        $('.selected').removeClass('selected ui-state-hover');
        $('i.feedSelect').removeClass('icon-checked').addClass('icon-checkbox');

        PTL.tab.saveTabs();
      }
      }).disableSelection();

    $colButtons
      .append($colLegend,
        $colDelButton,
        $colNewButton)
      .addClass('ui-state-disabled');

    return $column;
  }
};
